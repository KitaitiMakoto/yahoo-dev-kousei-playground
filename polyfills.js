// https://gist.github.com/rtoal/5d7e3fb3946486c0bd31d23f2bb7b4b8
// Polyfill because Safari's HTMLCollections are not iterable
if (typeof HTMLCollection.prototype[Symbol.iterator] !== 'function') {
  HTMLCollection.prototype[Symbol.iterator] = function () {
    let i = 0;
    return {
      next: () => ({done: i >= this.length, value: this.item(i++)})
    }
  };
}
