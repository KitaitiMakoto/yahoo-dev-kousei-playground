'use scrict';

import _ from './polyfills';
import Tooltip from 'tooltip.js';

document.addEventListener('DOMContentLoaded', () => {
  let inits = [initApp()];
  if (/debug/.test(location.search)) {
    inits.push(setMockSentence());
    inits.push(setMockForm());
  }
  Promise.all(inits)
    .then(([app,]) => {
      app.form.querySelector('[type="submit"]').disabled = false;
    })
    .catch(error => {
      console.error(error);
    });
});

function setMockSentence() {
  return fetch('data/request2.json')
    .then(response => response.json())
    .then(mock => {
      for (let key in mock) {
        for (let field of document.querySelectorAll(`[name="${key}"]`)) {
          field.value = mock[key];
        }
      }

      return mock;
    });
}

function setMockForm() {
  let form = document.getElementsByTagName('form')[0];
  form.action = 'data/response2.xml';
  form.method = 'get';

  return Promise.resolve(form);
}

function initApp() {
  let app = new App(document.getElementsByTagName('form')[0], document.getElementsByClassName('result')[0]);

  return Promise.resolve(app);
}

class App {
  constructor(form, result) {
    this.form = form;
    this.addFormListener();
    this.result = result;
  }

  addFormListener() {
    this.form.addEventListener('submit', event => {
      let body = new URLSearchParams();
      for (let field of this.form.querySelectorAll('[name]')) {
        body.append(field.name, field.value);
      }
      let fetchOptions = {
        method: this.form.method.toUpperCase(),
        mode: 'cors',
        body: body
      };
      if (this.form.method === 'get') {
        delete fetchOptions.body;
      }
      fetch(this.form.action, fetchOptions)
        .then(response => response.text())
        .then(text => {
          let parser = new DOMParser();
          return parser.parseFromString(text, 'application/xml');
        })
        .then(doc => {
          let resultSet = doc.children[0];
          let results = [];
          for (let result of resultSet.children) {
            results.push(new Result(result));
          }

          this.showResult(results);
        })
        .catch(error => {
          console.error(error);
          alert(error.message);
        });
      event.preventDefault();
    });
  }

  showResult(results) {
    results.sort((res1, res2) => res2.startPos - res1.startPos);
    this.result.textContent = this.form.sentence.value;
    let container = this.result.childNodes[0];
    let resultViews = [];
    for (let result of results) {
      let range = document.createRange();
      range.setStart(container, result.startPos);
      range.setEnd(container, result.startPos + result.length);
      let resultView = new ResultView(result);
      range.surroundContents(resultView.element);
      resultViews.push(resultView);
    }
    resultViews.forEach(rv => rv.show());

    location.href = '#result';
  }
}

class Result {
  constructor(element) {
    for (let elem of element.children) {
      switch (elem.tagName) {
      case 'StartPos':
        this.startPos = parseInt(elem.textContent);
        break;
      case 'Length':
        this.length = parseInt(elem.textContent);
        break;
      case 'Surface':
        this.surface = elem.textContent;
        break;
      case 'ShitekiWord':
        this.shitekiWord = elem.textContent;
        break;
      case 'ShitekiInfo':
        this.shitekiInfo = elem.textContent;
        break;
      }
    }
  }
}

class ResultView {
  constructor(result) {
    this.result = result;
    this.element = this.createElement();
    this.bindTooltip();
  }

  createElement() {
    let elem = document.createElement('i');
    elem.classList.add('shiteki');
    elem.dataset.shitekiInfo = result.shitekiInfo;
    elem.dataset.shitekiWord = result.shitekiWord;

    return elem;
  }

  bindTooltip() {
    this.tooltip = new Tooltip(this.element, {
      title: `${this.result.surface}[${this.result.shitekiInfo}]\n${this.result.shitekiWord}`,
      placement: 'bottom',
      trigger: 'click'
    });
  }

  show() {
    this.tooltip.show();
  }
}
